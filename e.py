#!/usr/bin/env python3

def get_int(s):
    get_input = input(s)
    while not get_input.lstrip('-').isdigit():
        get_input = input(s)
    return int(get_input)

def factorial(n):
    if n < 0:
        return "undefined"
    ans = 1
    for i in range(1, n + 1):
        ans *= i
    return ans

def e(m = 6):
    return sum([(2 * n + 2) / factorial(2 * n + 1) for n in range(m)])

print(e(get_int("How many iterations? ")))
